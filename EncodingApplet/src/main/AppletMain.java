package main;
import java.applet.Applet;
import java.awt.Button;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.security.PrivateKey;
import java.security.PublicKey;

import rsa.AsymmetricCryptography;

/**
 * The Class AppletMain.
 */
public class AppletMain extends Applet implements ActionListener, ItemListener {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3304407479752305222L;
	
	private Label label; 
	private TextField tf1, tf2, tf3, tf4;
	private Button button;
	
	public void init() {
		this.setSize(800, 200);
		
		label = new Label("Enter your text");
		tf1= new TextField(10);

		tf2= new TextField(100);

		button = new Button("Transform");
		
		tf3= new TextField(20);
		tf4= new TextField(20);
		
		add(label);
		add(tf1);
		
		add(button);
		
		add(tf2);
		
		add(tf3);
		add(tf4);
		
		tf1.addActionListener(this); 
		tf2.addActionListener(this);
		button.addActionListener(this);
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		repaint();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == button) {
			try {
				AsymmetricCryptography ac = new AsymmetricCryptography();
				PrivateKey privateKey = ac.getPrivate("privateKey");
				PublicKey publicKey = ac.getPublic("publicKey");
	
				String msg = tf1.getText();
				
				long startTime;
				long stopTime;
				
				startTime = System.currentTimeMillis();
				String encrypted_msg = ac.encryptText(msg, privateKey);
				stopTime = System.currentTimeMillis();
				tf3.setText(startTime - stopTime + "");
				
				startTime = System.currentTimeMillis();
				String decrypted_msg = ac.decryptText(encrypted_msg, publicKey);
				stopTime = System.currentTimeMillis();
				tf4.setText(stopTime - startTime + "");
				
				System.out.println("Original Message: " + msg + 
					"\nEncrypted Message: " + encrypted_msg
					+ "\nDecrypted Message: " + decrypted_msg);
				
				tf2.setText(encrypted_msg);
				
				File output = new File("criptare.txt");
				ac.encryptFile(msg.getBytes(), output,privateKey);
	
			} catch (Exception exception) {
				exception.getMessage();
			}
		}
		
		repaint();
	}

}
